﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingState : CharacterState
{
    
    bool jump = false;

    public FallingState(Character character, CharacterStateMachine characterStateMachine) : base(character, characterStateMachine){}

    public override void Enter()
    {
        base.Enter();
        character.anim.SetBool("fall", true);
        jump = false;
        if(character.jumpCount == 0)
        {
            character.jumpCount = 1;
        }
        character.enemyHeadTouched = false;
    }

    public override void Exit()
    {
        base.Exit();
        character.anim.SetBool("fall", false);
    }

    public override void HandleInput()
    {
        base.HandleInput();
        if(character.jumpCount != 2)
        {
            jump = Input.GetButtonDown("Jump");
        }        
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (jump)
        {
            characterStateMachine.ChangeState(character.doubleJumpState);
        }
        if (character.GetGroundStatus() && !character.hit)
        {
            characterStateMachine.ChangeState(character.groundedState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
