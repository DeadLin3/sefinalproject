﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingState : CharacterState
{
    private bool jump = false;

    public JumpingState (Character character, CharacterStateMachine characterStateMachine) : base(character, characterStateMachine)
    {

    }

    public override void Enter()
    {
        base.Enter();
        character.anim.SetBool("jump", true);
        character.Jump();
        character.jumpCount = 1;
    }

    public override void Exit()
    {
        base.Exit();
        character.anim.SetBool("jump", false);
    }

    public override void HandleInput()
    {
        base.HandleInput();
        jump = Input.GetButtonDown("Jump");
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (jump)
        {
            characterStateMachine.ChangeState(character.doubleJumpState);
        }
        if (!character.GetGroundStatus())
        //if (character.rb.velocity.y < 0)
        {
            characterStateMachine.ChangeState(character.fallingState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
