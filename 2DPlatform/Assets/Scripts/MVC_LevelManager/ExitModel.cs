﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitModel
{
    private ExitMVC mvc;

    public int interactedLeverCounter { get; private set; } = 0;
    public int necessaryLeverNumber { get; private set; } = 5;
    public bool doorOpenStatus { get; private set; } = false;

    public ExitModel()
    {
        necessaryLeverNumber = GameObject.FindGameObjectsWithTag("Lever").Length;
    }

    public void SetMVC(ExitMVC mvc)
    {
        this.mvc = mvc;
    }

    public void IncreaseLeverCounter()
    {
        interactedLeverCounter++;
        CheckIfOpen();
    }

    private void CheckIfOpen()
    {
        if (interactedLeverCounter == necessaryLeverNumber)
        {
            mvc.door.UpdateStatus();
        }
    }

}
