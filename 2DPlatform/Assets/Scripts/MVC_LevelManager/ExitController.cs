﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitController 
{
    public ExitMVC mvc { get; private set; }

    public void SetMVC(ExitMVC mvc)
    {
        this.mvc = mvc;
    }

    public void InteractWithLever(LeverView leverView)
    {
        leverView.UpdateStatus();
        mvc.model.IncreaseLeverCounter(); 
    }


    public void LoadNextLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void ShowVictoryScreen(GameObject victoryPanel)
    {
        Time.timeScale = 0;
        victoryPanel.SetActive(true);
    }
}
