﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeetCollider : MonoBehaviour
{
    private Character character;

    private void Start()
    {
        character = transform.parent.GetComponent<Character>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {            
        if (collision.transform.tag != "Player")
        {
            character.SetGroundStatus(true);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag != "Player")
        {
            character.SetGroundStatus(false);
        }
    }


}
