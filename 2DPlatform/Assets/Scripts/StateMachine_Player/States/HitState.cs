﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitState : CharacterState
{
    private float timer = 0;

    public HitState(Character character, CharacterStateMachine csMachine) : base(character, csMachine) { }

    public override void Enter()
    {
        base.Enter();
        character.anim.SetBool("fall", false);
        character.anim.SetBool("run", false);
        //character.anim.SetBool("run", false);
        character.anim.SetBool("hit", true);
        character.GetHit();
        character.HitJump();
        timer += Time.time;
    }

    public override void HandleInput()
    {
        base.HandleInput();
        horizontalInput = 0f;
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (!character.GetGroundStatus() && Time.time - timer >= 0.1f) //+ qualcosa
        {
            characterStateMachine.ChangeState(character.fallingState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void Exit()
    {
        //Debug.Log("NVM I'm Fine!");
        base.Exit();
        character.anim.SetBool("hit", false);
        character.hitCount = 0;
        character.hit = false;
        timer = 0;
    }
}