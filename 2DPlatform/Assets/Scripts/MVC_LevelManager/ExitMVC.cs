﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitMVC
{
    public readonly ExitController controller;
    public readonly ExitModel model;
    public readonly List<IView> levers = new List<IView>();
    public readonly IView door;

    public ExitMVC(ExitController controller, ExitModel model, List<IView> levers, IView door)
    {
        this.controller = controller;
        this.model = model;
        this.levers = levers;
        this.door = door;

        controller.SetMVC(this);
        model.SetMVC(this);
        door.SetMVC(this);
        foreach(IView v in levers)
        {
            v.SetMVC(this);
        }
    }

}
