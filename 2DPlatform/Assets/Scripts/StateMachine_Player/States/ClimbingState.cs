﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ClimbingState : CharacterState
{
    public ClimbingState(Character character, CharacterStateMachine csMachine) : base(character, csMachine) { }

    public override void Enter()
    {
        
        base.Enter();
        character.rb.gravityScale = 0;
        Physics2D.IgnoreCollision(character.collider, GameObject.FindObjectOfType<TilemapCollider2D>(), true);
        Physics2D.IgnoreCollision(character.gameObject.transform.GetChild(0).gameObject.GetComponent<BoxCollider2D>(), GameObject.FindObjectOfType<TilemapCollider2D>(), true);
        //character.rb.simulated = false;
    }

    public override void HandleInput()
    {
        base.HandleInput();
        verticalInput = Input.GetAxisRaw("Vertical");
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        //if (character.isOnStairs == false)
        if (character.GetGroundStatus() || character.isOnStairs == false)
        {
            characterStateMachine.ChangeState(character.groundedState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
        
        character.StairsMovement(verticalInput);
    }

    public override void Exit()
    {
        base.Exit();
        character.rb.gravityScale = 3;
        Physics2D.IgnoreCollision(character.collider, GameObject.FindObjectOfType<TilemapCollider2D>(), false);
        Physics2D.IgnoreCollision(character.gameObject.transform.GetChild(0).gameObject.GetComponent<BoxCollider2D>(), GameObject.FindObjectOfType<TilemapCollider2D>(), false);
        //character.rb.simulated = true;        
    }
}
