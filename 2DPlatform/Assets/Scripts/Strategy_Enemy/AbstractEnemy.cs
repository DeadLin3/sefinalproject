﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbstractEnemy : IEnemy
{
    private IMoveBehaviour moveBehaviour;
    private int healthPoints = 2;

    private bool isChasing = false;
    private bool alive = true;
    private bool dying = false;

    public AbstractEnemy(IMoveBehaviour moveBehaviour, int healthPoints)
    {
        this.moveBehaviour = moveBehaviour;
        this.healthPoints = healthPoints;
    }

    public void Move()
    {
        if (!dying)
        {
            if (isChasing)
                moveBehaviour.Chase();
            else
                moveBehaviour.IdleMove();
        }
        
    }

    public void SetMoveBehaviour(IMoveBehaviour moveBehaviour)
    {
        this.moveBehaviour = moveBehaviour;
    }

    public void SetChasing(bool status)
    {
        isChasing = status;
    }

    public void SetDying(bool status)
    {
        dying = status;
    }

    public void GetHit()
    {
        healthPoints--;
        if (healthPoints == 0)
        {
            alive = false;
        }
    }

    public bool isAlive()
    {
        return alive;
    }
}
