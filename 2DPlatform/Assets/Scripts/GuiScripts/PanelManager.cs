﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PanelManager : MonoBehaviour, IObserver
{
    [SerializeField] private GameObject pauseUI;
    [SerializeField] private GameObject gameOverUI;
    [SerializeField] private GameObject[] hearts;
    [SerializeField] private GameObject potionText;
    [SerializeField] private Animator animator = null;

    private Character character;

    private void Start()
    {
        Time.timeScale = 1;
        try
        {
            character = FindObjectOfType<Character>();
        }
        catch(System.Exception e)
        {
            character = null;
        }

        if(character != null)
        {
            potionText.GetComponent<Text>().text = "Trigger all the 5 levers and win the level!";
            potionText.GetComponent<Animator>().SetTrigger("playAnimation");
        }

    }

    private void Update()
    {
        if (character != null)
        {
            switch (character.playerHP)
            {
                case 2:
                    hearts[2].active = false;
                    break;
                case 1:
                    hearts[1].active = false;
                    break;
                case 0:
                    hearts[0].active = false;
                    gameOverUI.SetActive(true);
                    Time.timeScale = 0f;
                    break;
            }
        }
    }

    public void Pause()
    {
        pauseUI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        pauseUI.SetActive(false);
        Time.timeScale = 1f;
    }

    public void SceneLoader(int SceneIndex)
    {
        SceneManager.LoadScene(SceneIndex);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void SelectLevel() 
    {
        bool choice = animator.GetBool("selection");
        animator.SetBool("selection", !choice);
    }

    public void Notify(Event e)
    {
        switch (e)
        {
            case Event.Hit:
                potionText.GetComponent<Text>().text = "All enemies received 1 dmg!";
                potionText.GetComponent<Animator>().SetTrigger("playAnimation");
                break;
        }
    }
}
