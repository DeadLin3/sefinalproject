﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningState : CharacterState
{
    protected bool jump;

    public RunningState (Character character, CharacterStateMachine characterStateMachine) : base(character, characterStateMachine) {}

    public override void Enter()
    {
        base.Enter();
        character.anim.SetBool("run", true);
    }

    public override void Exit()
    {
        base.Exit();        
        character.anim.SetBool("run", false);
    }

    public override void HandleInput()
    {
        base.HandleInput();
        jump = Input.GetButtonDown("Jump");
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (horizontalInput == 0)
        {
            characterStateMachine.ChangeState(character.groundedState);
        }
        if (jump)
        {
            characterStateMachine.ChangeState(character.jumpingState);
        }
        if (!character.GetGroundStatus() && !character.hit)
        {
            characterStateMachine.ChangeState(character.fallingState);
        }
        if (!character.GetGroundStatus() && !character.isOnStairs)
        {
            characterStateMachine.ChangeState(character.fallingState);
        }
        
        if (character.isOnStairs && verticalInput != 0)
        {
            characterStateMachine.ChangeState(character.climbingState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
