﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverView : MonoBehaviour, IView
{
    private ExitMVC mvc;

    private GameObject levelOff;
    private GameObject levelOn;

    private AudioManager audioManager;

    public void SetMVC(ExitMVC mvc)
    {
        this.mvc = mvc;
    }

    private void Start()
    {
        levelOff = gameObject.transform.GetChild(0).gameObject;
        levelOn = gameObject.transform.GetChild(1).gameObject;

        levelOff.SetActive(true);
        levelOn.SetActive(false);

         audioManager = FindObjectOfType<AudioManager>();
    }
       
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (levelOff.activeSelf && collision.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                mvc.controller.InteractWithLever(this);               
            }
        }
    }

    public void UpdateStatus()
    {
        if (levelOff.activeSelf)
        {
            audioManager.Play("Lever");
            levelOff.SetActive(false);
            levelOn.SetActive(true);
        }
        else
        {
            audioManager.Play("Lever");
            levelOff.SetActive(true);
            levelOn.SetActive(false);
        }
    }
}
