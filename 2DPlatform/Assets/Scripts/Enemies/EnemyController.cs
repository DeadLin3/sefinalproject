﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour, IObserver
{
    private IObservable observable = null;

    private IEnemy enemyType;
    private Animator animator;
    private Rigidbody2D rb;
    private AudioSource deathSound;

    public void SetEnemyType(IEnemy enemyType)
    {
        this.enemyType = enemyType;
    }

    public void SetObservable(IObservable observable)
    {
        this.observable = observable;
        observable.Register(this);
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        deathSound = GetComponent<AudioSource>();
        
    }

    private void Update()
    {
        enemyType.Move();
    }

    public void SetChasing(bool status)
    {
        enemyType.SetChasing(status);
    }

    public void GetHit()
    {
        enemyType.GetHit();
        if (!enemyType.isAlive())
        {
            PrepareForDeath();
        }
        else
        {
            animator.SetTrigger("hit");
        } 
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Spike")
        {
            PrepareForDeath();
        }
    }

    public void PrepareForDeath()
    {
        enemyType.SetDying(true);
        deathSound.Play();
        animator.SetTrigger("die");
        rb.simulated = false;
    }

    public void Die()
    {
        if (observable != null)
        {
            observable.Remove(this);
        }        
        Destroy(gameObject);
    }

    public void Notify(Event e)
    {
        switch (e)
        {
            case Event.Hit:
                GetHit();
                break;
            case Event.Flee:
                //
                break;
        }
    }
}
