﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractObservable : MonoBehaviour, IObservable
{
    private List<IObserver> observers = new List<IObserver>();

    public void NotifyObservers(Event e)
    {
        foreach (IObserver o in observers)
        {
            o.Notify(e);
        }
    }

    public void Register(IObserver observer)
    {
        observers.Add(observer);
    }

    public void Remove(IObserver observer)
    {
        observers.Remove(observer);
    }
}
