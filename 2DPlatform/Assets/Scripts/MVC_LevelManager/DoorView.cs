﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorView : MonoBehaviour, IView
{
    private ExitMVC mvc;

    private GameObject doorClosed;
    private GameObject doorOpen;

    public void SetMVC(ExitMVC mvc)
    {
        this.mvc = mvc;
    }

    private void Start()
    {
        doorClosed = gameObject.transform.GetChild(0).gameObject;
        doorOpen = gameObject.transform.GetChild(1).gameObject;

        doorClosed.SetActive(true);
        doorOpen.SetActive(false);
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        if (doorOpen.activeSelf && collision.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                mvc.controller.LoadNextLevel("SecondLevel");
            }
        }
    }*/

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (doorOpen.activeSelf && collision.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Time.timeScale = 0;
                mvc.controller.LoadNextLevel("SecondLevel");
            }
        }
    }

    public void UpdateStatus()
    {
        if (doorClosed.activeSelf)
        {
            doorClosed.SetActive(false);
            doorOpen.SetActive(true);
        }
        else
        {
            doorClosed.SetActive(true);
            doorClosed.SetActive(false);
        }
    }
}
