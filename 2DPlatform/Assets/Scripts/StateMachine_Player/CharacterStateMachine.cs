﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStateMachine
{
    public CharacterState currentState { get; private set; }

    public void Initialize(CharacterState startingState)
    {
        currentState = startingState;
        startingState.Enter();
    }

    public void ChangeState(CharacterState newState)
    {
        currentState.Exit();

        currentState = newState;
        newState.Enter();
    }
}
