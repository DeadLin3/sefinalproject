﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpOnHeadManager : MonoBehaviour
{
    private EnemyController enemyController;
    private Character character;

    private void Start()
    {
        enemyController = transform.parent.gameObject.GetComponent<EnemyController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerFeet")
        {
            enemyController.GetHit();
            character = collision.transform.parent.gameObject.GetComponent<Character>();
            character.enemyHeadTouched = true;
            //StartCoroutine("ResetHeadTouched", character);
            character.characterStateMachine.ChangeState(character.jumpingState);
        }
    }

    IEnumerator ResetHeadTouched(Character character)
    {
        yield return new WaitForSeconds(0.5f);
        character.enemyHeadTouched = false;
    }
}
