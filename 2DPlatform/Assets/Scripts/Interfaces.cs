﻿#region Observer Pattern

public interface IObserver
{
    void Notify(Event e);
}

public interface IObservable
{
    void Register(IObserver observer);
    void Remove(IObserver observer);
    void NotifyObservers(Event e);
}

#endregion 

#region State Machine Pattern

public interface IState
{
    void Enter();
    void HandleInput();
    void LogicUpdate();
    void PhysicsUpdate();
    void Exit();
}

#endregion

#region MVC Pattern

public interface IView
{
    void SetMVC(ExitMVC mvc);
    void UpdateStatus();
}

#endregion

#region Strategy Pattern

public interface IMoveBehaviour
{
    void IdleMove();
    void Chase();
}

public interface IEnemy
{
    void Move();
    void SetChasing(bool status);
    void SetDying(bool status);
    void GetHit();
    bool isAlive();
    void SetMoveBehaviour(IMoveBehaviour moveBehaviour);
}

#endregion