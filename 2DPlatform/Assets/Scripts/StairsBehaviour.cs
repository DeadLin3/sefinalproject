﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsBehaviour : MonoBehaviour
{

    private Character character;

    // Start is called before the first frame update
    void Start()
    {
        character = FindObjectOfType<Character>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            character.isOnStairs = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            character.isOnStairs = false;
        }
    }

}
