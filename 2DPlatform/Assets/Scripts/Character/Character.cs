﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public enum FacingDirection { Left, Right }

    #region State Machine variables
    public CharacterStateMachine characterStateMachine;
    public GroundedState groundedState;
    public RunningState runningState;
    public JumpingState jumpingState;
    public DoubleJumpState doubleJumpState;
    public FallingState fallingState;
    public ClimbingState climbingState;
    public HitState hitState;
    #endregion

    #region Class variables

    public int playerHP = 3;
    [HideInInspector] public bool hit = false;
    [HideInInspector] public int hitCount = 0;
    public Animator anim;
    public FacingDirection facingDirection = FacingDirection.Right;
    [HideInInspector] public Rigidbody2D rb;
    public int jumpCount = 0;
    [HideInInspector] public bool isOnStairs = false;
    [HideInInspector] public CapsuleCollider2D collider { get; private set; }
    private Collider2D feetCollider;
    [HideInInspector] public bool enemyHeadTouched = false;
    [HideInInspector] public AudioManager audioManager;

    private float runSpeed = 5;
    private Vector2 jumpforce = new Vector2(0, 9);
    private Vector2 hitJumpforce = new Vector2(-2, 9);
    private bool grounded = true;
    
    

    #endregion

    #region MonoBehaviour Callbacks
    // Start is called before the first frame update
    void Start()
    {
        #region State Machine setup
        characterStateMachine = new CharacterStateMachine();
        groundedState = new GroundedState(this, characterStateMachine);
        runningState = new RunningState(this, characterStateMachine);
        jumpingState = new JumpingState(this, characterStateMachine);
        doubleJumpState = new DoubleJumpState(this, characterStateMachine);
        fallingState = new FallingState(this, characterStateMachine);
        climbingState = new ClimbingState(this, characterStateMachine);
        hitState = new HitState(this, characterStateMachine);

        characterStateMachine.Initialize(groundedState);
        #endregion

        rb = GetComponent<Rigidbody2D>();
        collider = GetComponent<CapsuleCollider2D>();
        feetCollider = GetComponent<BoxCollider2D>();

        audioManager = FindObjectOfType<AudioManager>();

    }
    // Update is called once per frame
    void Update()
    {
        characterStateMachine.currentState.HandleInput();
        characterStateMachine.currentState.LogicUpdate();

        //CheckGrounded();
    }

    private void FixedUpdate()
    {
        characterStateMachine.currentState.PhysicsUpdate();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "GroundEnemy"
            || collision.gameObject.tag == "FlyingEnemy"
            || collision.gameObject.tag == "JumpingEnemy"
            || collision.gameObject.tag == "Spike")
        {
            if (!enemyHeadTouched)
            {
                hit = true;
            }
                
        }
    }    

    #endregion

    #region Methods

    public void UpdateDirection(FacingDirection dir)
    {
        if (facingDirection != dir)
        {
            facingDirection = dir;

            if (facingDirection == FacingDirection.Right)
            {
                transform.forward = new Vector3(0, 0, 1);
            }
            else
            {
                transform.forward = new Vector3(0, 0, -1);
            }
        }
    }

    public void Move(float axisValue)
    {
        float movement = axisValue * runSpeed * Time.deltaTime;
        transform.position += new Vector3(movement, 0, 0);
    }

    public void Jump()
    {
        audioManager.Play("Jump");
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(jumpforce, ForceMode2D.Impulse);
    }

    public void HitJump()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(hitJumpforce, ForceMode2D.Impulse);
    }

    public void StairsMovement(float axisValue)
    {
        float movement = axisValue * runSpeed * Time.deltaTime;
        transform.position += new Vector3(0, movement, 0);
    }

    public void SetGroundStatus(bool status)
    {
        grounded = status;
    }

    public bool GetGroundStatus()
    {
        return grounded;
    }

    public void GetHit()
    {
        audioManager.Play("Hit");
        playerHP--;
        hitCount++;
    }
    #endregion
}
