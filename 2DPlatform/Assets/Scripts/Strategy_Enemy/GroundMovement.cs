﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GroundMovement : BaseMovement
{
    public GroundMovement(GameObject owner, float offset, float speed, GameObject target) : base(owner,offset,speed,target) {}

    public override void IdleMove()
    {
        base.IdleMove();

        owner.transform.position = new Vector3(speed * Time.deltaTime + owner.transform.position.x , owner.transform.position.y, owner.transform.position.z);
    }

    public override void Chase()
    {
        base.Chase();

        owner.transform.position = new Vector3(speed * Time.deltaTime + owner.transform.position.x, owner.transform.position.y, owner.transform.position.z);
    }

}
