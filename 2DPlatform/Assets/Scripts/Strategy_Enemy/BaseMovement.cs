﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMovement : IMoveBehaviour
{
    protected Vector3 initialPosition;
    protected float offset = 2;
    protected GameObject target;
    protected GameObject owner;
    protected float speed = 2f;

    public BaseMovement(GameObject owner, float initialPositionOffset, float speed, GameObject target)
    {
        this.owner = owner;
        offset = initialPositionOffset;
        this.speed = speed;
        initialPosition = owner.transform.position;
        this.target = target;

    }

    public virtual void Chase()
    {
        Vector3 diff = new Vector3(target.transform.position.x - owner.transform.position.x, target.transform.position.y - owner.transform.position.y, 0);
        if (diff.x < 0)
        {
            speed = -Mathf.Abs(speed);
            owner.transform.forward = new Vector3(0, 0, -1);
        }
        else
        {
            speed = Mathf.Abs(speed);
            owner.transform.forward = new Vector3(0, 0, 1);
        }
    }

    public virtual void IdleMove()
    {
        if (owner.transform.position.x <= initialPosition.x - offset)
        {
            if (owner.transform.forward.z == -1)
            {
                speed = Mathf.Abs(speed);
                owner.transform.forward = new Vector3(0, 0, 1);
            }
        }
        else if (owner.transform.position.x >= initialPosition.x + offset)
        {
            if (owner.transform.forward.z == 1)
            {
                speed = -Mathf.Abs(speed);
                owner.transform.forward = new Vector3(0, 0, -1);
            }
        }
    }

}
