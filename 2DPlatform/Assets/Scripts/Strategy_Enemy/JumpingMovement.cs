﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingMovement : BaseMovement
{
    private EnemyController enemyController;
    private Rigidbody2D rb;

    private float timer = 0;
    private Vector2 jumpForce;
    private Vector2 chasingForce;

    public JumpingMovement(GameObject owner, float offset, float speed, GameObject target) : base(owner, offset, speed, target)
    {
        enemyController = owner.GetComponent<EnemyController>();
        rb = owner.GetComponent<Rigidbody2D>();
        jumpForce = new Vector2(1f * Mathf.Abs(speed), 3 * Mathf.Abs(speed));
    }

    public override void Chase()
    {
        base.Chase();

        timer += Time.deltaTime;
        if (timer >= 1.5f)
        {
            timer -= 1.5f;
            chasingForce = new Vector2((Mathf.Min(jumpForce.x, Mathf.Abs(target.transform.position.x - owner.transform.position.x))) * Mathf.Sign(speed),
                                        jumpForce.y);
            rb.AddForce(chasingForce, ForceMode2D.Impulse);
        }

    }

    public override void IdleMove()
    {
        base.IdleMove();

        timer += Time.deltaTime;
        if (timer >= 2.0f)
        {
            timer -= 2.0f;
            rb.AddForce(new Vector2(jumpForce.x * Mathf.Sign(speed), jumpForce.y), ForceMode2D.Impulse);
        }
        
        

    }
}
