﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : AbstractObservable
{
    // Start is called before the first frame update
    void Start()
    {
        EnemyController[] enemyControllers = FindObjectsOfType<EnemyController>();
        foreach (EnemyController e in enemyControllers)
        {
            e.SetObservable(this);
        }
        PanelManager panelManager = FindObjectOfType<PanelManager>();
        Register(panelManager);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            NotifyObservers(Event.Hit);
            Destroy(gameObject);
        }
    }
}
