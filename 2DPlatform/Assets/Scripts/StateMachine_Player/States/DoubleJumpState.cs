﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJumpState : CharacterState
{
    public DoubleJumpState(Character character, CharacterStateMachine characterStateMachine) : base(character, characterStateMachine)
    {

    }

    public override void Enter()
    {
        base.Enter();
        character.anim.SetBool("djump", true);
        character.Jump();
        character.jumpCount = 2;
        character.enemyHeadTouched = false;
    }

    public override void Exit()
    {
        base.Exit();
        character.anim.SetBool("djump", false);
    }

    public override void HandleInput()
    {
        base.HandleInput();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (!character.GetGroundStatus() && character.rb.velocity.y < -0.75)
        {
            characterStateMachine.ChangeState(character.fallingState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }
}
