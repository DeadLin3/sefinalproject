﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : AbstractEnemy
{
    public FlyingEnemy(FlyingMovement flyingMovement, int healthPoints) : base(flyingMovement, healthPoints) { }
}
