﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    private AudioManager audioManager;

    private void Awake()
    {
         audioManager = FindObjectOfType<AudioManager>();
    }

    private void Start()
    {
        audioManager.StopAllSounds();
        audioManager.Play("Menu");
    }
}
