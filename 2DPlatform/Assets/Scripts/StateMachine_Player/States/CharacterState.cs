﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterState : IState
{
    protected Character character;
    protected CharacterStateMachine characterStateMachine;
    protected float horizontalInput;
    protected float verticalInput;

    protected CharacterState( Character character, CharacterStateMachine characterStateMachine)
    {
        this.character = character;
        this.characterStateMachine = characterStateMachine;
    }

    public virtual void Enter()
    {
        //Debug.Log("" + this.ToString());
    }

    public virtual void HandleInput()
    {
        horizontalInput = Input.GetAxisRaw("Horizontal");
    }

    public virtual void LogicUpdate()
    {
        if (horizontalInput > 0)
        {
            character.UpdateDirection(Character.FacingDirection.Right);
        }
        if (horizontalInput < 0)
        {
            character.UpdateDirection(Character.FacingDirection.Left);
        }
        /*if (character.enemyHeadTouched)
        {
            characterStateMachine.ChangeState(character.jumpingState);
        }*/
        if (character.hit && character.hitCount == 0)
        {
            characterStateMachine.ChangeState(character.hitState);
        }
    }

    public virtual void PhysicsUpdate()
    {
        character.Move(horizontalInput);
    }

    public virtual void Exit()
    {

    }

}


