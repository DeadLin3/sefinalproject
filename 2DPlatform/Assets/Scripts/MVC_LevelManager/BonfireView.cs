﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonfireView : MonoBehaviour, IView
{
    private ExitMVC mvc;

    private GameObject animation;
    [SerializeField] private GameObject victoryPanel;

    public void SetMVC(ExitMVC mvc)
    {
        this.mvc = mvc;
    }

    private void Start()
    {
        animation = gameObject.transform.GetChild(0).gameObject;

        animation.SetActive(false);
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (animation.activeSelf && collision.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                mvc.controller.ShowVictoryScreen(victoryPanel);
            }
        }
    }

    public void UpdateStatus()
    {
        if (animation.activeSelf)
        {
            animation.SetActive(false);
        }
        else
        {
            animation.SetActive(true);
        }
    }
}
