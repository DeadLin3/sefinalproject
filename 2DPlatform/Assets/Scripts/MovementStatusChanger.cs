﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementStatusChanger : MonoBehaviour
{
    private EnemyController eCon;

    private void Start()
    {
        eCon = gameObject.transform.parent.gameObject.GetComponent<EnemyController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            eCon.SetChasing(true);
        }
    }
}
