﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public ExitMVC exitMVC { get; private set; }
    public List<IView> leverViews { get; private set; } = new List<IView>();
    public IView doorView { get; private set; }
    public IView bonfireView { get; private set; }
    public ExitModel exitModel { get; private set; }
    public ExitController exitController { get; private set; }

    private AudioManager audioManager;
    

    private void Awake()
    {
        #region MVC setup

        exitModel = new ExitModel();
        exitController = new ExitController();

        try
        {
            doorView = GameObject.FindGameObjectWithTag("Door").GetComponent<DoorView>();
        }
        catch (NullReferenceException ex)
        {
            doorView = null;
        }
        try
        {
            bonfireView = GameObject.FindGameObjectWithTag("Bonfire").GetComponent<BonfireView>();
        }
        catch (NullReferenceException ex)
        {
            bonfireView = null;
        }      

        GameObject[] leverList = GameObject.FindGameObjectsWithTag("Lever");
        foreach (GameObject g in leverList)
        {
            leverViews.Add(g.GetComponent<LeverView>());
        }

        if (doorView != null)
        {
            exitMVC = new ExitMVC(exitController, exitModel, leverViews, doorView);
        }
        if (bonfireView != null)
        {
            exitMVC = new ExitMVC(exitController, exitModel, leverViews, bonfireView);
        }

        #endregion

        #region Setup Enemies
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        #region Setup Ground Enemies
        {
            GameObject[] groundEnemies = GameObject.FindGameObjectsWithTag("GroundEnemy");
            for (int i = 0; i < groundEnemies.Length; i++)
            {
                EnemyController enemyController = groundEnemies[i].GetComponent<EnemyController>();
                GroundMovement groundMovement = new GroundMovement(groundEnemies[i], 2f, 2f, player);
                enemyController.SetEnemyType(new GroundEnemy(groundMovement, 2));
            }
        }
        #endregion

        #region Setup Flying Enemies
        {
            GameObject[] flyingEnemies = GameObject.FindGameObjectsWithTag("FlyingEnemy");
            for (int i = 0; i < flyingEnemies.Length; i++)
            {
                EnemyController enemyController = flyingEnemies[i].GetComponent<EnemyController>();
                FlyingMovement flyingMovement = new FlyingMovement(flyingEnemies[i], 2f, 2f, player);
                enemyController.SetEnemyType(new FlyingEnemy(flyingMovement, 1));
                flyingEnemies[i].GetComponent<Rigidbody2D>().gravityScale = 0;
            }
        }
        #endregion

        #region Setup Jumping Enemies
        {
            GameObject[] jumpingEnemies = GameObject.FindGameObjectsWithTag("JumpingEnemy");
            for (int i = 0; i < jumpingEnemies.Length; i++)
            {
                EnemyController enemyController = jumpingEnemies[i].GetComponent<EnemyController>();
                JumpingMovement jumpingMovement  = new JumpingMovement(jumpingEnemies[i], 2f, 2f, player);
                enemyController.SetEnemyType(new JumpingEnemy(jumpingMovement, 3));
            }
        }
        #endregion

        #endregion

        audioManager = FindObjectOfType<AudioManager>();

    }

    private void Start()
    {
        

        int sceneIndex = SceneManager.GetActiveScene().buildIndex;

        switch (sceneIndex)
        {
            case 1:
                audioManager.StopAllSounds();
                audioManager.Play("Level1");
                break;
            case 2:
                audioManager.StopAllSounds();
                audioManager.Play("Level2");
                break;
        }
    }
}
