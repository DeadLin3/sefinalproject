﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedState : CharacterState
{
    protected bool jump;    

    public GroundedState(Character character, CharacterStateMachine characterStateMachine) : base(character, characterStateMachine)
    {

    }

    public override void Enter()
    {
        base.Enter();
        character.jumpCount = 0;
        //character.enemyHeadTouched = false;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void HandleInput()
    {
        base.HandleInput();
        jump = Input.GetButtonDown("Jump");
        verticalInput = Input.GetAxisRaw("Vertical");
        
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if(horizontalInput != 0)
        {
            characterStateMachine.ChangeState(character.runningState);
        }
        if (jump)
        {
            characterStateMachine.ChangeState(character.jumpingState);
        }
        if (!character.GetGroundStatus() && !character.isOnStairs)
        {
            characterStateMachine.ChangeState(character.fallingState);
        }
        if(character.isOnStairs && verticalInput != 0)
        {
            characterStateMachine.ChangeState(character.climbingState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    
}
