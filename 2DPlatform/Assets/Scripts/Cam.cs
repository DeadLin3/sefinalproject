﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam : MonoBehaviour
{
    public Transform charTransform;
    private Transform myTransform;

    private void Start()
    {
        myTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        myTransform.position = new Vector3(charTransform.position.x, charTransform.position.y + 0.5f, -6f);
        
    }
}
