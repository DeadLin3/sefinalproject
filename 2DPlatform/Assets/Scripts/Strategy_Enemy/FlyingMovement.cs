﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingMovement : BaseMovement
{
    public FlyingMovement(GameObject owner, float offset, float speed, GameObject target) : base (owner, offset, speed, target) {}

    public override void Chase()
    {
        base.Chase();

        owner.transform.position = new Vector3(owner.transform.position.x + speed * Time.deltaTime, 
                                                owner.transform.position.y + Time.deltaTime * (target.transform.position.y - owner.transform.position.y), 
                                                owner.transform.position.z);
    }

    public override void IdleMove()
    {
        base.IdleMove();

        owner.transform.position = new Vector3(owner.transform.position.x + speed * Time.deltaTime,
                                                owner.transform.position.y + Mathf.Sin(Time.time * 6) * Time.deltaTime * 2,
                                                owner.transform.position.z);
        
    }
}
